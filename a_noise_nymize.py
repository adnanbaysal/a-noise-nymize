from datetime import datetime
import numpy as np
from matplotlib import pyplot as plt


class UnrecognizedDistributionName(Exception):
    pass


class DataStats:
    def __init__(self, data) -> None:
        self.data = data
        self.N = len(data)
        self.mean = sum(data) / self.N
        self.median = np.median(data)
        self.min = min(data)
        self.max = max(data)

    def __str__(self) -> str:
        return f"N: {self.N}\nmean: {self.mean}\nmedian: {self.median}\nmin: {self.min}\nmax: {self.max}\n"


def gen_random_data(distribution="normal", mean=25, spread=12.5, num_rows=1000):
    """
    returns a random database of size num_rows with the given distribution and parameters
    distribution: one of the values "normal", "uniform", or "laplace"
    mean: mean of the values in the database
    spread: For the normal distribution, it is the standard deviation, 
            hence more than %95 of the values lies between (mean - 2*spread, mean + 2*spread).
            For the uniform distribution, spread is the distance from mean to min and mean to max.
            For the laplace distribution, spread is the scale factor
    num_rows: number of rows in the output
    """

    if distribution == "normal":
        return np.random.normal(mean, spread, num_rows)
    elif distribution == "uniform":
        return np.random.uniform(mean - spread, mean + spread, num_rows)
    elif distribution == "laplace":
        return np.random.laplace(mean, spread, num_rows)
    else:
        raise UnrecognizedDistributionName()


def calculate_histogram_error(db: DataStats, private_db: DataStats, min_val, max_val):
    """
    Creates histograms for both dbs with common bin intervals. 
    min_val, max_val represents the minimun and maximum values in both databases
    Then, for each bin, frequencies are calculated and frequency absolute differences are returned.
    The sum may be implemented as chi-squared sums, i.e. instead of summing rhe absolute values,
    square of the differences may be added. 
    """
    hist_db = np.histogram(db.data, bins=10, range=(min_val, max_val))[0]
    hist_private = np.histogram(
        private_db.data, bins=10, range=(min_val, max_val))[0]
    diff_dist = [abs(hist_db[i] - hist_private[i]) for i in range(10)]
    # sum divided by N for normalization
    return sum(diff_dist) / db.N


def calculate_query_errors(db: DataStats, private_db):
    """
    This function compares the original db with anonimized private_db in terms of some statistical functions.
    The following comparison functions are applied:
        1) mean: mean of both databases are compared
        2) median: median of both databases are compared
        3) min: difference between min function output
        4) max: difference between max function output
        5) histogram: Values are put into 10 bins and absolute values of frequency differences are added 
    """
    mean_error = db.mean - private_db.mean
    median_error = db.median - private_db.median
    min_error = db.min - private_db.min
    max_error = db.max - private_db.max

    min_val = min(db.min, private_db.min)
    max_val = max(db.max, private_db.max)
    hist_error = calculate_histogram_error(db, private_db, min_val, max_val)

    return {
        "mean_error": mean_error,
        "median_error": median_error,
        "min_error": min_error,
        "max_error": max_error,
        "hist_error": hist_error
    }


def analyze_error_distribution(db: DataStats, distribution="uniform", spread=1.0, T=5000):
    """
    This function calculates the distribution of differences between outputs of some query 
    functions between the original original database and random noise added privatized version.
    First a datebase is created, then T randomized versions are computed. For each randomized 
    version, 5 query functions are computed on each database and the difference between them 
    are output as erron for that query. Finally, a histogram is output for each query type. 
    """
    N = db.N

    metric_errors = {
        "mean_error": [],
        "median_error": [],
        "min_error": [],
        "max_error": [],
        "hist_error": []
    }

    for t in range(T):
        if t % 500 == 0:
            print(f"      {int(t / 500) + 1} / 10")
        noise = gen_random_data(distribution=distribution,
                                mean=0, spread=spread, num_rows=N)
        private_db = DataStats(db.data + noise)
        result = calculate_query_errors(db, private_db)
        for key, value in result.items():
            metric_errors[key].append(value)

    # save analysis result to file for further process
    ofile = open(f"metric_errors_N{N}_{distribution}.py", "w")
    ofile.write(f"metric_errors_N{N}_{distribution} = {str(metric_errors)}")
    ofile.close()


def create_base_charts():
    start = datetime.now()

    for N in [1000, 10000, 100000]:
        db = DataStats(gen_random_data(distribution="normal",
                                       mean=25, spread=12.5, num_rows=N))
        print("-------------------------------------------------")
        print(f"N = {N}:")
        for distribution in ["uniform", "normal", "laplace"]:
            print(f"   {distribution} noise:")
            analyze_error_distribution(db, distribution=distribution)

    print("elapsed time:", datetime.now() - start)


if __name__ == "__main__":
    create_base_charts()
    from metric_errors_N1000_laplace import metric_errors_N1000_laplace
    from metric_errors_N1000_normal import metric_errors_N1000_normal
    from metric_errors_N1000_uniform import metric_errors_N1000_uniform

    from metric_errors_N10000_laplace import metric_errors_N10000_laplace
    from metric_errors_N10000_normal import metric_errors_N10000_normal
    from metric_errors_N10000_uniform import metric_errors_N10000_uniform

    from metric_errors_N100000_laplace import metric_errors_N100000_laplace
    from metric_errors_N100000_normal import metric_errors_N100000_normal
    from metric_errors_N100000_uniform import metric_errors_N100000_uniform

    for key in metric_errors_N1000_uniform.keys():
        fig, axs = plt.subplots(3, 3)
        fig.suptitle(key.replace("_", " ").title())

        axs[0, 0].hist(metric_errors_N1000_uniform[key], bins=20)
        axs[0, 0].set_title('N = 1000')
        axs[0, 0].set(ylabel="Uniform noise")
        axs[0, 1].hist(metric_errors_N10000_uniform[key], bins=20)
        axs[0, 1].set_title('N = 10000')
        axs[0, 1].sharey(axs[0, 0])
        axs[0, 2].hist(metric_errors_N100000_uniform[key], bins=20)
        axs[0, 2].set_title('N = 100000')
        axs[0, 2].sharey(axs[0, 0])

        axs[1, 0].hist(metric_errors_N1000_normal[key], bins=20)
        axs[1, 0].set(ylabel="Normal noise")
        axs[1, 1].hist(metric_errors_N10000_normal[key], bins=20)
        axs[1, 1].sharey(axs[1, 0])
        axs[1, 2].hist(metric_errors_N100000_normal[key], bins=20)
        axs[1, 2].sharey(axs[1, 0])

        axs[2, 0].hist(metric_errors_N1000_laplace[key], bins=20)
        axs[2, 0].set(ylabel="Laplace noise")
        axs[2, 1].hist(metric_errors_N10000_laplace[key], bins=20)
        axs[2, 1].sharey(axs[2, 0])
        axs[2, 2].hist(metric_errors_N100000_laplace[key], bins=20)
        axs[2, 2].sharey(axs[2, 0])

        fig.tight_layout()

        figure = plt.gcf()
        figure.set_size_inches(8, 6)
        plt.savefig(f"{key}.png", dpi=120)
        plt.clf()
