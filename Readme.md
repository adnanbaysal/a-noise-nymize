# A-noise-nymize: Anonymize by Random Noise

This project analyzed different random noise adding mechanisms to protect privacy of entries in a one-column database. 

## Environment setup

`pipenv` is necessary to setup the environment. You can install pipenv with `pip install pipenv`

Then run the following to activate the shell:

`pipenv shell`

To install dependecies, run:

`pipenv install`

## Running the analyzes

Simply run the following while the environment is active:

`python a_noise_nymize.py`

The whole analysis completes in about 7 minutes and histogram images are output to root folder.

## Sample output

![alt text](outputs/max_error.png "Mean function error distribution when adding uniform noise in [-1, 1]")

You can find other sample outputs in the `/outputs` folder. 

## Dependecies

`numpy`

`matplotlib`
